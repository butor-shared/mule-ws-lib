/**
 *  Copyright (C) 2016 Butor Inc. All rights reserved.
 *  Butor Inc. PROPRIETARY/CONFIDENTIAL.
 *  Use is subject to license terms.
 *
 *  http://www.butor.com
 *
 *  (info@butor.com)
 */
/**
 * 
 */
package com.butor.ws;

import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.apache.ws.security.WSPasswordCallback;

/**
 * @author asawan
 *
 */
public class ClientPasswordCallback implements CallbackHandler {
	public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
		WSPasswordCallback pc = (WSPasswordCallback) callbacks[0];

		if ("joe".equals(pc.getIdentifier())) {
			pc.setPassword("joespassword");
		} // else {...} - can add more users, access DB, etc.
	}
}
