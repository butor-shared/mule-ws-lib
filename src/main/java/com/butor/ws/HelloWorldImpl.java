/**
 *  Copyright (C) 2016 Butor Inc. All rights reserved.
 *  Butor Inc. PROPRIETARY/CONFIDENTIAL.
 *  Use is subject to license terms.
 *
 *  http://www.butor.com
 *
 *  (info@butor.com)
 */
package com.butor.ws;

import javax.jws.WebService;

/**
 * @author asawan
 *
 */

@WebService(endpointInterface = "org.example.HelloWorld", serviceName = "HelloWorld")
public class HelloWorldImpl implements HelloWorld {
	@Override
	public String sayHi(String text) {
		return "Hello " + text;
	}
	@Override
	public String sayHello(String text) {
		return "Hello " + text;
	}
	@Override
	public Long getAge() {
		return 50L;
	}
}