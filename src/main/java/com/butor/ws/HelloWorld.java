/**
 *  Copyright (C) 2016 Butor Inc. All rights reserved.
 *  Butor Inc. PROPRIETARY/CONFIDENTIAL.
 *  Use is subject to license terms.
 *
 *  http://www.butor.com
 *
 *  (info@butor.com)
 */
package com.butor.ws;

import javax.jws.WebService;

/**
 * @author asawan
 *
 */

@WebService
public interface HelloWorld {
	String sayHi(String text);
	String sayHello(String text);
	Long getAge();
}