/**
 *  Copyright (C) 2016 Butor Inc. All rights reserved.
 *  Butor Inc. PROPRIETARY/CONFIDENTIAL.
 *  Use is subject to license terms.
 *
 *  http://www.butor.com
 *
 *  (info@butor.com)
 */
/**
 * 
 */
package com.butor.ws;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.configuration.security.FiltersType;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.apache.cxf.ws.security.wss4j.WSS4JOutInterceptor;
import org.apache.ws.security.WSConstants;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author asawan
 *
 */
public class WsClient {
	private Logger logger = LoggerFactory.getLogger(getClass());

	public static void main(String[] args) {
		try {
			new WsClient().call();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void call() throws NoSuchAlgorithmException, CertificateException, FileNotFoundException, IOException,
			KeyStoreException, UnrecoverableKeyException {
		JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
		factory.getInInterceptors().add(new LoggingInInterceptor());
		factory.getOutInterceptors().add(new LoggingOutInterceptor());

		// server side security
		// Map<String,Object> inProps = new HashMap<String,Object>();
		// //... how to configure the properties is outlined below;
		//
		// WSS4JInInterceptor wssIn = new WSS4JInInterceptor(inProps);
		// factory.getOutInterceptors().add(wssIn);

		// Client side security
		// Map<String,Object> outProps = new HashMap<String,Object>();
		// ... how to configure the properties is outlined below (http://cxf.apache.org/docs/ws-security.html)

		Map<String, Object> outProps = new HashMap<String, Object>();
		outProps.put(WSHandlerConstants.ACTION, WSHandlerConstants.USERNAME_TOKEN);
		outProps.put(WSHandlerConstants.USER, "joe");
		outProps.put(WSHandlerConstants.PASSWORD_TYPE, WSConstants.PW_TEXT);
		outProps.put(WSHandlerConstants.PW_CALLBACK_REF, new ClientPasswordCallback()); // ClientPasswordCallback.class.getName()

		WSS4JOutInterceptor wssOut = new WSS4JOutInterceptor(outProps);
		factory.getOutInterceptors().add(wssOut);

		factory.setServiceClass(HelloWorld.class);
		factory.setAddress("https://localhost:8083/hello");
		HelloWorld service = (HelloWorld) factory.create();

		Client client = ClientProxy.getClient(service);

		// How to configure the HTTPConduit for the SOAP Client?
		// http://cxf.apache.org/docs/client-http-transport-including-ssl-support.html
		// ---
		// First you need get the HTTPConduit from the Proxy object or Client,
		// then you can set the HTTPClientPolicy,
		// AuthorizationPolicy,
		// ProxyAuthorizationPolicy,
		// TLSClientParameters, and/or HttpBasicAuthSupplier.
		final HTTPConduit httpConduit = (HTTPConduit) client.getConduit();
		final HTTPClientPolicy httpClientPolicy = httpConduit.getClient();

		// HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();

		httpClientPolicy.setConnectionTimeout(36000);
		httpClientPolicy.setAllowChunking(false);
		httpClientPolicy.setReceiveTimeout(32000);

		// http.setClient(httpClientPolicy);

		/*
		 * <http:conduit name="{http://apache.org/hello_world}HelloWorld.http-conduit">
		 * 
		 * <http:tlsClientParameters> <sec:keyManagers keyPassword="password"> <sec:keyStore type="JKS"
		 * password="password" file="my/file/dir/Morpit.jks"/> </sec:keyManagers> <sec:trustManagers> <sec:keyStore
		 * type="JKS" password="password" file="my/file/dir/Truststore.jks"/> </sec:trustManagers>
		 * <sec:cipherSuitesFilter> <!-- these filters ensure that a ciphersuite with export-suitable or null encryption
		 * is used, but exclude anonymous Diffie-Hellman key change as this is vulnerable to man-in-the-middle attacks
		 * --> <sec:include>.*_EXPORT_.*</sec:include> <sec:include>.*_EXPORT1024_.*</sec:include>
		 * <sec:include>.*_WITH_DES_.*</sec:include> <sec:include>.*_WITH_AES_.*</sec:include>
		 * <sec:include>.*_WITH_NULL_.*</sec:include> <sec:exclude>.*_DH_anon_.*</sec:exclude> </sec:cipherSuitesFilter>
		 * </http:tlsClientParameters> <http:authorization> <sec:UserName>Betty</sec:UserName>
		 * <sec:Password>password</sec:Password> </http:authorization> <http:client AutoRedirect="true"
		 * Connection="Keep-Alive"/>
		 * 
		 * </http:conduit>
		 */
		TLSClientParameters tls = new TLSClientParameters();
		// howto generate stores and export/import certificates :
		// https://docs.mulesoft.com/mule-user-guide/v/3.7/tls-configuration

		// TODO disable CN/hostname check tempo
		// tls.setDisableCNCheck(true);

		final String keyStoreType = "JKS";
		final String keyPassword = "server1234";
		final KeyStore keyStore = KeyStore.getInstance(keyStoreType);
		keyStore.load(new FileInputStream("/data/workspace-butor/mule-ws/src/main/resources/server-localhost.jks"),
				keyPassword.toCharArray());

		KeyManagerFactory keyFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
		keyFactory.init(keyStore, keyPassword.toCharArray());
		KeyManager[] km = keyFactory.getKeyManagers();
		tls.setKeyManagers(km);

		final String trustStoreType = keyStoreType;
		final String trustStorePassword = "client1234";
		final KeyStore trustStore = KeyStore.getInstance(trustStoreType);
		trustStore.load(
				new FileInputStream("/data/workspace-butor/mule-ws/src/main/resources/client-localhost_truststore.jks"),
				trustStorePassword.toCharArray());

		TrustManagerFactory trustFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		trustFactory.init(trustStore);
		TrustManager[] tm = trustFactory.getTrustManagers();

		tls.setTrustManagers(tm);

		FiltersType ft = new FiltersType();
		List<String> cs = ft.getInclude();
		cs.add(".*_EXPORT_.*");
		cs.add(".*_EXPORT1024_.*");
		cs.add(".*_WITH_DES_.*");
		cs.add(".*_WITH_AES_.*");
		cs.add(".*_WITH_NULL_.*");

		cs = ft.getExclude();
		cs.add(".*_DH_anon_.*");

		httpConduit.setTlsClientParameters(tls);

		String reply = service.sayHi("Sam");
		System.out.println("Server said: " + reply);
		System.exit(0);
	}
}
